## Uitwerkingen Terraform prutssessie

Het uitchecken van een git-branch, brengt je naar de oplossing van de opdracht

| Branch | Opdracht |
| ------------- |-------------|
| opdr01 | Installatie en providers |
| opdr02 | SSH keypair configureren |
| opdr03 | Security groups |
| opdr04 | Refactoring: variabelen |
| opdr05 | Refactoring: credentials |
| opdr06 | Instance |
| opdr07 | Loadbalancer |
| opdr08 | Autoscaling |
| opdr09 | S3 bucket (object storage) |
| opdr10 | Remote statefiles |
